import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

declare var $:JQueryStatic;

@Component({
    selector: 'login-form',
    templateUrl: 'app/login/login.form.component.html',
    styleUrls: ['app/login/login.form.component.css']
})

export class LoginFormComponent implements AfterViewInit {

    constructor(
        private router: Router
    ) {}

    @ViewChild('particles') el:ElementRef;

    ngAfterViewInit(): void {


        (<any>$("#particles")).particleground({
            dotColor: '#e2e2e2',
            lineColor: '#e2e2e2',
            particleRadius: 7, // Dot size
            lineWidth: 1,
            proximity: 100, // How close two dots need to be before they join
        });

        $('.intro').css({
            'margin-top': -($('.intro').height() / 2)
        });
    }

    onSubmit() {
        console.log("Submitted!");
        this.router.navigate(['/adminpage']);
    }


}
