"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var LoginFormComponent = (function () {
    function LoginFormComponent(router) {
        this.router = router;
    }
    LoginFormComponent.prototype.ngAfterViewInit = function () {
        $("#particles").particleground({
            dotColor: '#e2e2e2',
            lineColor: '#e2e2e2',
            particleRadius: 7,
            lineWidth: 1,
            proximity: 100,
        });
        $('.intro').css({
            'margin-top': -($('.intro').height() / 2)
        });
    };
    LoginFormComponent.prototype.onSubmit = function () {
        console.log("Submitted!");
        this.router.navigate(['/adminpage']);
    };
    return LoginFormComponent;
}());
__decorate([
    core_1.ViewChild('particles'),
    __metadata("design:type", core_1.ElementRef)
], LoginFormComponent.prototype, "el", void 0);
LoginFormComponent = __decorate([
    core_1.Component({
        selector: 'login-form',
        templateUrl: 'app/login/login.form.component.html',
        styleUrls: ['app/login/login.form.component.css']
    }),
    __metadata("design:paramtypes", [router_1.Router])
], LoginFormComponent);
exports.LoginFormComponent = LoginFormComponent;
//# sourceMappingURL=login.form.component.js.map