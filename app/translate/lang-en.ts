export const LANG_EN_NAME = 'en';

export const LANG_EN_TRANS = {
    'hello world': 'hello world',
    'menuManager': 'MENU MANAGER',
    'addSection': 'Add Section',
    'delete': 'Delete',
    'save': 'Save',
    'selectUnselectAll': 'Select/Unselect All',
    'deleteConfirmationQuestion': 'Would you like to delete selected items?',
    'ok': 'Ok',
    'cancel': 'Cancel'
};