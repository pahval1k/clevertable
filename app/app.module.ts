import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { RouterModule }   from '@angular/router';
import { HttpModule, RequestOptions } from '@angular/http';
import { AppComponent }   from './app.component';
import { LoadingSpinner } from './common/components/loading_spinner/loading.spinner'
import { ConfirmationDialog } from './common/components/confirmation_dialog/confirmation_dialog'
import { LoginFormComponent }   from './login/login.form.component';
import { AdminPageComponent }   from './admin_page/admin.page.component';
import { toolBarComponent }   from './admin_page/md_toolbar/md.toolbar.component';
import { contentComponent }   from './admin_page/md_content/md.content.component';
import { menuManagerComponent } from './admin_page/components/menu_manager/menu.manager.component';
import { menuItemComponent } from './admin_page/components/menu_manager/menu_item_component/menu.item.component';
import {menuToolComponent} from "./admin_page/components/menu_manager/menu_tool_component/menu.tool.component";
import {modalComponent} from "./admin_page/components/menu_manager/modal_component/modal.component";
import { ContentBodyDirective } from './admin_page/components/menu_manager/modal_component/components/content.body.directive';
import { reviewItemComponent } from './admin_page/components/menu_manager/modal_component/components/review.item.component';
import { addOrEditItemComponent } from './admin_page/components/menu_manager/modal_component/components/add.or.edit.item.component';
import { CustomRequestOptions } from './common/CustomRequestOptions';
import { TRANSLATION_PROVIDERS }   from './translate/translation';
import { TranslatePipe }   from './translate/translate.pipe';
import { TranslateService }   from './translate/translate.service';


@NgModule({
    imports:      [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot([
            { path: '', component: LoginFormComponent },
            { path: 'adminpage', component: AdminPageComponent },
        ])
    ],
    declarations: [
        AppComponent,
        TranslatePipe,
        LoadingSpinner,
        ConfirmationDialog,
        LoginFormComponent,
        AdminPageComponent,
        toolBarComponent,
        contentComponent,
        menuManagerComponent,
        menuItemComponent,
        menuToolComponent,
        modalComponent,
        ContentBodyDirective,
        reviewItemComponent,
        addOrEditItemComponent
    ],
    providers: [
        { provide: RequestOptions, useClass: CustomRequestOptions },
        TRANSLATION_PROVIDERS,
        TranslateService
    ],
    bootstrap:    [ AppComponent ],
    entryComponents: [reviewItemComponent, addOrEditItemComponent]
})

export class AppModule { }