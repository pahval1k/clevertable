"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var http_1 = require("@angular/http");
var app_component_1 = require("./app.component");
var loading_spinner_1 = require("./common/components/loading_spinner/loading.spinner");
var confirmation_dialog_1 = require("./common/components/confirmation_dialog/confirmation_dialog");
var login_form_component_1 = require("./login/login.form.component");
var admin_page_component_1 = require("./admin_page/admin.page.component");
var md_toolbar_component_1 = require("./admin_page/md_toolbar/md.toolbar.component");
var md_content_component_1 = require("./admin_page/md_content/md.content.component");
var menu_manager_component_1 = require("./admin_page/components/menu_manager/menu.manager.component");
var menu_item_component_1 = require("./admin_page/components/menu_manager/menu_item_component/menu.item.component");
var menu_tool_component_1 = require("./admin_page/components/menu_manager/menu_tool_component/menu.tool.component");
var modal_component_1 = require("./admin_page/components/menu_manager/modal_component/modal.component");
var content_body_directive_1 = require("./admin_page/components/menu_manager/modal_component/components/content.body.directive");
var review_item_component_1 = require("./admin_page/components/menu_manager/modal_component/components/review.item.component");
var add_or_edit_item_component_1 = require("./admin_page/components/menu_manager/modal_component/components/add.or.edit.item.component");
var CustomRequestOptions_1 = require("./common/CustomRequestOptions");
var translation_1 = require("./translate/translation");
var translate_pipe_1 = require("./translate/translate.pipe");
var translate_service_1 = require("./translate/translate.service");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            router_1.RouterModule.forRoot([
                { path: '', component: login_form_component_1.LoginFormComponent },
                { path: 'adminpage', component: admin_page_component_1.AdminPageComponent },
            ])
        ],
        declarations: [
            app_component_1.AppComponent,
            translate_pipe_1.TranslatePipe,
            loading_spinner_1.LoadingSpinner,
            confirmation_dialog_1.ConfirmationDialog,
            login_form_component_1.LoginFormComponent,
            admin_page_component_1.AdminPageComponent,
            md_toolbar_component_1.toolBarComponent,
            md_content_component_1.contentComponent,
            menu_manager_component_1.menuManagerComponent,
            menu_item_component_1.menuItemComponent,
            menu_tool_component_1.menuToolComponent,
            modal_component_1.modalComponent,
            content_body_directive_1.ContentBodyDirective,
            review_item_component_1.reviewItemComponent,
            add_or_edit_item_component_1.addOrEditItemComponent
        ],
        providers: [
            { provide: http_1.RequestOptions, useClass: CustomRequestOptions_1.CustomRequestOptions },
            translation_1.TRANSLATION_PROVIDERS,
            translate_service_1.TranslateService
        ],
        bootstrap: [app_component_1.AppComponent],
        entryComponents: [review_item_component_1.reviewItemComponent, add_or_edit_item_component_1.addOrEditItemComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map