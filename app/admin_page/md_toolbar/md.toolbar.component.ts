import { Component } from '@angular/core';
import { LoadingSpinner } from '../../common/components/loading_spinner/loading.spinner'

@Component({
    selector: 'md-toolbar',
    templateUrl: 'app/admin_page/md_toolbar/md.toolbar.component.html',
    styleUrls: ['app/admin_page/md_toolbar/md.toolbar.component.css']
})

export class toolBarComponent {

    menuItems: Array<string>;
    activeItemIndex: number;
    isLoading: boolean;

    constructor() {
        this.menuItems = ['1','2','3'];
        this.activeItemIndex = 0;
        this.isLoading = false;
    }

    setActiveItem = (index) => {
        this.activeItemIndex = index;
        this.isLoading = true;
        setTimeout(() => {
            this.isLoading = false;
        }, 2000);

    };

    hideToolBar = () => {
        $('#chapter-list').removeClass('slide-chapter-in').addClass('slide-chapter-out').delay(300).queue(function(next) {
            $(this).removeClass('show').addClass('hidden');
            $('#clickMenu').removeClass('menu-out').delay(200).queue(function(next) {
                $(this).children().first().removeClass('hide-menu-icon').removeClass('hidden').addClass('show-menu-icon');
                next();
            });
            next();
        });
    };

    ngAfterViewInit(): void {
        (<any>$('#clickMenu')).on('click', function() {
            if ($(this).hasClass('menu-out')) {
                /*$('#chapter-list').removeClass('slide-chapter-in').addClass('slide-chapter-out').delay(300).queue(function(next) {
                    $(this).removeClass('show').addClass('hidden');
                    $('#clickMenu').removeClass('menu-out').delay(200).queue(function(next) {
                        $(this).children().first().removeClass('hide-menu-icon').removeClass('hidden').addClass('show-menu-icon');
                        next();
                    });
                    next();
                });*/
            } else {
                $(this).children().first().removeClass('show-menu-icon').addClass('hide-menu-icon').delay(100).queue(function(next) {
                    $(this).addClass('hidden').parent().addClass('menu-out').delay(300).queue(function(next) {
                        $('#chapter-list').removeClass('hidden').addClass('show').removeClass('slide-chapter-out').addClass('slide-chapter-in');
                        next();
                    });
                    next();
                });
            }
        });
    }



}
