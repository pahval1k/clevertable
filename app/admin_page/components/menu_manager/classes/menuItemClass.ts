import { modalContentType } from "../enums/modal.conent.type"

export class menuItemClass {

    _id: number;
    title: string;
    description: string;
    imagePath: string;
    categoryId: number;
    inheritanceLevel: number;
    subItems: Array<menuItemClass>;
    visible: boolean;
    action: modalContentType;
    selected: boolean;
    price: string;
    subItemOf: number;

    constructor(id: number = -1, title: string = "", description: string = "", imagePath: string = "", categoryId: number = 1, inheritanceLevel: number = 1, subItems: Array<menuItemClass> = [], selected: boolean = false, price: string = "", subItemOf: number = 0) {
        this._id = id;
        this.title = title;
        this.description = description;
        this.imagePath = imagePath;
        this.categoryId = categoryId;
        this.inheritanceLevel = inheritanceLevel;
        this.subItems = subItems;
        this.visible = true;
        this.selected = selected;
        this.price = price;
        this.subItemOf = subItemOf;

    }
}