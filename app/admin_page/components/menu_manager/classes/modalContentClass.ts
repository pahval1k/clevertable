import { modalContentType } from '../enums/modal.conent.type'

export class modalContentClass {

    modalVisible: boolean;
    title: string;
    body: string;
    confirmButtonText: string;
    rejectButtonText: string;
    type: modalContentType;

    constructor(modalVisible: boolean = false, title: string = "", body: string = "", confirmButtonText: string = "", rejectButtonText: string = "", type: modalContentType = undefined) {
        this.modalVisible = modalVisible;
        this.title = title;
        this.body = body;
        this.confirmButtonText = confirmButtonText;
        this.rejectButtonText = rejectButtonText;
        this.type = type
    }
}