import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { menuItemClass } from '../classes/menuItemClass'
import { modalContentType } from '../enums/modal.conent.type'
import { DisplayModalService } from '../services/displayModalService'

@Component({
    selector: 'menu-item',
    templateUrl: 'app/admin_page/components/menu_manager/menu_item_component/menu.item.component.html',
    styleUrls: ['app/admin_page/components/menu_manager/menu_item_component/menu.item.component.css']
})

export class menuItemComponent implements OnInit{
    @Input()
    menuItem: menuItemClass;
    @Output()
    displayModal: EventEmitter<menuItemClass> = new EventEmitter<menuItemClass>();
    @Output()
    sendDisplayingUp: EventEmitter<menuItemClass> = new EventEmitter<menuItemClass>();
    @Output()
    checkItemEvent: EventEmitter<menuItemClass> = new EventEmitter<menuItemClass>();
    hideSubSection: boolean;
    expandState: string;
    private inheritanceLevelEvent: number;


    constructor(private displayService: DisplayModalService) {
        this.hideSubSection = false;
        this.expandState = "expand_less";
        this.inheritanceLevelEvent = 0;

    }

    ngOnInit() {}

    /*getMarginLevel(inheritanceLevel: number): number {
        let defaultMargin = 55;
        return defaultMargin * (inheritanceLevel - 1);
    }*/

    expandSubSection(): void {
        this.hideSubSection ? this.expandState = "expand_less" : this.expandState = "expand_more";
        this.hideSubSection = !this.hideSubSection;
    }

    checkItem(itemToCheck: menuItemClass): void {
        this.checkItemEvent.emit(itemToCheck)
    }

    removeItem(itemToRemove: menuItemClass): void {
        this.sendItemToHandle(itemToRemove, modalContentType.removeItem);
    }

    addItem(itemToAddIn: menuItemClass): void {
        let itemTOADD = new menuItemClass(null, null, null, null, null, itemToAddIn.inheritanceLevel + 1, null, false, null, itemToAddIn._id)
        this.sendItemToHandle(itemTOADD, modalContentType.addItem);
    }

    reviewItem(itemToReview: menuItemClass): void {
        this.sendItemToHandle(itemToReview, modalContentType.reviewItem);
    }

    editItem(itemToEdit: menuItemClass): void {
        this.sendItemToHandle(itemToEdit, modalContentType.editItem);
    }

    private sendItemToHandle(itemToHandle: menuItemClass, contentType: modalContentType): void {
        itemToHandle.action = contentType;
        this.displayService.setDisplay(itemToHandle);
    }

}