import { Component, Output, EventEmitter, OnInit} from '@angular/core';
import { MenuService } from './services/menuServices';
import { MenuHelper } from './helpers/menuHelper';
import { ModalContentFactory } from './services/modalContentFactory'
import { modalContentType } from "./enums/modal.conent.type";
import { modalContentClass } from './classes/modalContentClass';
import { menuItemClass } from "./classes/menuItemClass";
import { DisplayModalService } from "./services/displayModalService"
import { LoadingSpinner } from '../../../common/components/loading_spinner/loading.spinner'

@Component({
    selector: 'menu-manager',
    templateUrl: 'app/admin_page/components/menu_manager/menu.manager.component.html',
    styleUrls: ['app/admin_page/components/menu_manager/menu.manager.component.css'],
    providers: [MenuService, DisplayModalService, MenuHelper]
})

export class menuManagerComponent implements OnInit {
    menuItemsData: Array<menuItemClass>;
    modalContent: modalContentClass;
    filterValue: string;
    MenuService: MenuService;
    MenuHelper: MenuHelper;
    isLoading: boolean

    constructor(menuService: MenuService, menuHelper: MenuHelper) {
        this.MenuService = menuService;
        this.MenuHelper = menuHelper;
        this.menuItemsData = []
        this.filterValue = "";
        this.isLoading = false;
    }

    ngOnInit() {
        this.isLoading = true;
        setTimeout(() => {
            this.isLoading = false;
        }, 2000);

        this.MenuService.search().then((result) => {
            this.MenuHelper.setMenuItems(result)
            this.menuItemsData = this.MenuHelper.transformMenuItems(result)
        })
        .catch((error) => console.error(error));
    }

    doActionEvent(contentItem: menuItemClass) {
        switch (contentItem.action) {
            case modalContentType.removeItem:
                this.removeMenuItemById(contentItem);
                break;
            case modalContentType.editItem:
                this.editMenuItemById(contentItem);
                break;
            case modalContentType.addItem:
                this.addMenuItem(contentItem);
                break;
        }
    }

    addMenuItem(newMenuItem: menuItemClass) {
        this.menuItemsData = this.MenuHelper.addItem(newMenuItem);
    }

    editMenuItemById(menuItem: menuItemClass) {
        this.menuItemsData = this.MenuHelper.editItem(menuItem);
    }

    removeMenuItemById(menuItem: menuItemClass) {
        this.menuItemsData = this.MenuHelper.removeById(menuItem._id);
    }

    filterValueChange(filter: string) {
        this.menuItemsData = this.MenuHelper.filterByStr(filter);
    }

    selectAllEvent(state: boolean) {
        this.menuItemsData = this.MenuHelper.selectAll(state);
    }

    checkItem(item: menuItemClass) {
        this.menuItemsData = this.MenuHelper.checkItem(item);
    }

    removeAllSelectedItems() {
        this.menuItemsData = this.MenuHelper.removeAllSelectedItems();
    }

    hasSelected() {
        return this.MenuHelper.hasSelected();
    }

    saveMenu() {
        this.isLoading = true;
        setTimeout(() => {
            this.isLoading = false;
        }, 2000);
        this.MenuService.saveMenu(this.MenuHelper.getMenuItems())
    }


}