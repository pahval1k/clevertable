import { menuItemClass } from '../classes/menuItemClass';

export default class MenuTransform {

    constructor() {}

    entireMenuTransformLine(section: Array<any>): Array<menuItemClass>{

        if (!section || !section.length) return []

        let menuItemsData: menuItemClass[] = [];

        section.map((menuItem) => {
            let menuItemData = new menuItemClass(menuItem._id, menuItem.title, menuItem.description, menuItem.imagePath, menuItem.categoryId, menuItem.inheritanceLevel, menuItem.subItems, false, menuItem.price, menuItem.subItemOf);
            menuItemsData.push(menuItemData);
        })

        return menuItemsData;
    }

}