import { menuItemClass } from '../classes/menuItemClass';
import {Injectable} from "@angular/core";

@Injectable()
export class MenuHelper {

    menuItems: Array<menuItemClass>;

    constructor() {
        this.menuItems = []
    }

    setMenuItems(menuItems: Array<menuItemClass>) {
        this.menuItems = menuItems;
    }

    getMenuItems() {
        return this.menuItems
    }

    addItem(item: menuItemClass) {
        let iDS = this.menuItems.map((item) => item._id);
        let maxID = iDS && iDS.length ? Math.max.apply(null, iDS) : 0;
        let itemTOADD = new menuItemClass(maxID + 1, item.title, item.description, item.imagePath, item.categoryId, item.inheritanceLevel, [], item.selected, item.price, item.subItemOf)
        this.menuItems.push(itemTOADD);
        return this.transformMenuItems(this.menuItems);
    }

    selectAll(state: boolean) {
        let selectedItems = this.menuItems.map((menuItem) => {
            menuItem.selected = state
            return menuItem
        })
        return this.transformMenuItems(selectedItems);
    }

    filterByStr(str: string) {
        if (!str) return this.menuItems
        let filteredItems = this.menuItems.filter((menuItem) => menuItem.title.indexOf(str) !== -1)
        let newItems: Array<menuItemClass> = [];
        let newItem: menuItemClass;
        filteredItems.map((item) => {
            newItem = new menuItemClass(item._id, item.title, item.description, item.imagePath, item.categoryId, 1, [], item.selected, item.price, item.subItemOf)
            newItems.push(newItem);
        })
        return newItems
    }

    removeAllSelectedItems() {
        this.menuItems = this.menuItems.filter((menuItem) => !menuItem.selected)
        return this.transformMenuItems(this.menuItems);
    }

    removeById(id: number) {
        let index = this.menuItems.findIndex((item) => item._id === id)
        this.menuItems.splice(index, 1)
        return this.transformMenuItems(this.menuItems)
    }

    editItem(menuItem: menuItemClass) {
        let index = this.menuItems.findIndex((item) => item._id === menuItem._id)
        this.menuItems[index] = menuItem;
        return this.transformMenuItems(this.menuItems)
    }

    checkItem(item: menuItemClass) {
        let selectItem = this.menuItems.find((menuItem) => menuItem._id === item._id)
        let state = !selectItem.selected
        selectItem.selected = state
        this.checkSubItems(selectItem, state);
        return this.transformMenuItems(this.menuItems)
    }

    hasSelected() {
        return this.menuItems.filter((item) => item.selected).length
    }

    transformMenuItems(menuItems: Array<menuItemClass> = []) {

        let formattedItems = [];
        let filteredByInheritance = [];
        let maxInheritanceLevel = 3; // TODO: hardcoded. should be obtained from the server
        let startFrom = 1;
        let subItemsIds = [];
        let newItem: menuItemClass;

        for (let i = startFrom; i <= maxInheritanceLevel; i ++) {
            subItemsIds = [];
            filteredByInheritance = menuItems.filter((menuItem) => menuItem.inheritanceLevel === i)
            if (i === startFrom) { // first level
                filteredByInheritance.map((item) => {
                    newItem = new menuItemClass(item._id, item.title, item.description, item.imagePath, item.categoryId, item.inheritanceLevel, [], item.selected, item.price, item.subItemOf)
                    formattedItems.push(newItem);
                })
            } else {
                filteredByInheritance.map((item) => { subItemsIds.push(item.subItemOf) })
                subItemsIds = subItemsIds.filter((it, i, ar) => ar.indexOf(it) === i); // make unique
                subItemsIds.map((itemID) => {
                    let formattedItem = this.findByIndex(formattedItems, itemID)
                    if (formattedItem) {
                        let filteredById = filteredByInheritance.filter((item) => item.subItemOf === itemID)
                        filteredById.map((item) => {
                            newItem = new menuItemClass(item._id, item.title, item.description, item.imagePath, item.categoryId, item.inheritanceLevel, [], item.selected, item.price, item.subItemOf)
                            formattedItem.subItems.push(newItem);
                        })
                    }

                })
            }

        }

        return formattedItems;

    }

    private checkSubItems(item: menuItemClass, selectState: boolean) {
        let subItems = this.menuItems.filter((subItem) => subItem.subItemOf === item._id)
        let subIDS = []
        if (subItems) {
            subItems.map((item) => {
                item.selected = selectState
                subIDS.push(item._id)
            })
            subIDS.map((id) => {
                let newSubItems = this.menuItems.filter((subItem) => subItem.subItemOf === id)
                if (newSubItems && newSubItems.length) {
                    let newSubItem = this.menuItems.find((item) => item._id === id)
                    this.checkSubItems(newSubItem, selectState)
                }

            })
        }
    }

    private findByIndex(menuItems, index) {
        let menuItem: menuItemClass;
        for (let i = 0; i < menuItems.length; i++) {
            if (menuItems[i]._id === index) {
                menuItem = menuItems[i];
                break;
            } else if (menuItems[i].subItems.length) {
                menuItem = this.findByIndex(menuItems[i].subItems, index);
                if (menuItem) break;
            }
        }
        return menuItem;
    }

}