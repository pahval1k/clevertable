export enum modalContentType {
    removeItem,
    addItem,
    reviewItem,
    editItem,
    checkItem
}