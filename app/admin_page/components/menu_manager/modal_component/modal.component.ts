import {Component, Input, OnInit, Output, EventEmitter, ComponentFactoryResolver, ViewChild} from '@angular/core';
import { modalContentClass } from '../classes/modalContentClass';
import { modalContentType } from '../enums/modal.conent.type';
import { DisplayModalService } from "../services/displayModalService"
import { ContentBodyDirective } from './components/content.body.directive'
import { reviewItemComponent } from './components/review.item.component'
import { addOrEditItemComponent } from "./components/add.or.edit.item.component"
import { menuItemClass } from "../classes/menuItemClass";
import { ModalContentFactory } from "../services/modalContentFactory";

@Component({
    selector: 'modal-component',
    templateUrl: 'app/admin_page/components/menu_manager/modal_component/modal.component.html',
    styleUrls: ['app/admin_page/components/menu_manager/modal_component/modal.component.css']
})

export class modalComponent implements OnInit {
    @Input()
    modalContent: modalContentClass;
    @Output()
    doActionEvent: EventEmitter<menuItemClass> = new EventEmitter<menuItemClass>();
    @ViewChild(ContentBodyDirective) contentBodyDirective: ContentBodyDirective;
    itemToHandleObject: menuItemClass;

    constructor(private displayService: DisplayModalService, private _componentFactoryResolver: ComponentFactoryResolver) {}

    ngOnInit() {
        this.modalContent = new modalContentClass();
        this.displayService.getDisplay().subscribe((itemToHandle: menuItemClass) => {
            let modalContentInstance = new ModalContentFactory(itemToHandle.action);
            this.modalContent = modalContentInstance.getModalContent();
            this.itemToHandleObject = itemToHandle;
            if (itemToHandle.action !== modalContentType.removeItem) {
                let componentToDisplay = reviewItemComponent;

                if (itemToHandle.action === modalContentType.editItem || itemToHandle.action === modalContentType.addItem) {
                    componentToDisplay = addOrEditItemComponent;
                    if (itemToHandle.action === modalContentType.addItem) {
                        this.itemToHandleObject = new menuItemClass(null, "", "", "", itemToHandle.categoryId, itemToHandle.inheritanceLevel, null, false, null, itemToHandle.subItemOf);
                    }
                } else if (itemToHandle.action === modalContentType.reviewItem) {
                    componentToDisplay = reviewItemComponent;
                }
                let componentFactory = this._componentFactoryResolver.resolveComponentFactory(componentToDisplay);
                let viewContainerRef = this.contentBodyDirective.viewContainerRef;
                viewContainerRef.clear();
                let componentRef = viewContainerRef.createComponent(componentFactory);
                (componentRef.instance).data = this.itemToHandleObject;

            } else {
                //$(".modal-dynamic-content").siblings()[0].remove();
            }
        });
    }

    doAction(action: modalContentType) {
        this.itemToHandleObject.action = action;
        this.doActionEvent.emit(this.itemToHandleObject);
        this.modalContent.modalVisible = false;
    }

}