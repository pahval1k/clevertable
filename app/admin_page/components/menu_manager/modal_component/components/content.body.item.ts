import { Type } from '@angular/core';

export class ContentBodyItem {
    constructor(public component: Type<any>, public data: any) {}
}