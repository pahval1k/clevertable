import { Component, Input }  from '@angular/core';


@Component({
    template: `
   <div class="form-group">
     <label for="title" class="fld-required">Title</label>
     <input type="text" required class="form-control" id="title" [(ngModel)]="data.title">
   </div>
   <div class="form-group">
     <label for="price" class="fld-required">Price</label>
     <input type="text" required class="form-control" id="price" [(ngModel)]="data.price">
   </div>
   <div class="form-group">
     <label for="description" class="fld-required">Description</label>
     <textarea type="text" required class="form-control" id="description" [(ngModel)]="data.description"></textarea>
   </div>
`
})
export class addOrEditItemComponent {
    @Input() data: any;
}
