import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[content-body-directive]',
})
export class ContentBodyDirective {
    constructor(public viewContainerRef: ViewContainerRef) { }
}
