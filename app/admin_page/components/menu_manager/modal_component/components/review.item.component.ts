import { Component, Input }  from '@angular/core';


@Component({
    template: `
   <div class="alert alert-info" role="alert">{{data.title}}</div>
   <div class="alert alert-info" role="alert">{{data.price}}</div>
   <div class="alert alert-info" role="alert">{{data.description}}</div>
`
})
export class reviewItemComponent {
    @Input() data: any;
}
