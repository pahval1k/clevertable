export const htmlTemplate = `
   <div class="form-group fld-required">
     <label for="title">Title</label>
     <input type="text" required class="form-control" id="title" [(ngModel)]="itemToHandleObject.title">
   </div>
   <div class="form-group fld-required">
     <label for="price">Price</label>
     <input type="text" required class="form-control" id="price" [(ngModel)]="itemToHandleObject.price">
   </div>
   <div class="form-group fld-required">
     <label for="description">Description</label>
     <textarea type="text" required class="form-control" id="description" [(ngModel)]="itemToHandleObject.description"></textarea>
   </div>
`;