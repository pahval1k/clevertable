import { Component, Input, Output, EventEmitter } from '@angular/core';
import { menuItemClass } from '../classes/menuItemClass'
import { DisplayModalService } from '../services/displayModalService'
import { modalContentType } from "../enums/modal.conent.type";
import { ConfirmationDialog } from "../../../../common/components/confirmation_dialog/confirmation_dialog"

@Component({
    selector: 'menu-tool',
    templateUrl: 'app/admin_page/components/menu_manager/menu_tool_component/menu.tool.component.html',
    styleUrls: ['app/admin_page/components/menu_manager/menu_tool_component/menu.tool.component.css']
})

export class menuToolComponent {

    @Input()
    filterValue: string;
    @Input()
    allowDelete: boolean;
    @Output()
    filterValueChange: EventEmitter<string> = new EventEmitter<string>();
    @Output()
    selectAllEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output()
    removeSelectedEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output()
    saveMenuEvent: EventEmitter<void> = new EventEmitter<void>()
    @Output()
    addSectionEvent: EventEmitter<void> = new EventEmitter<void>()
    private state: boolean;
    private showConfirmationDialog: boolean

    constructor(private displayService: DisplayModalService) {
        this.state = false;
        this.showConfirmationDialog = false;
    }

    filterValueChanged(event) {
        this.filterValueChange.emit(this.filterValue);
    }

    selectAllAction() {
        this.state = !this.state;
        this.selectAllEvent.emit(this.state);
    }

    addSection() {
        var newItem = new menuItemClass()
        this.sendItemToHandle(newItem, modalContentType.addItem)
    }

    removeSelectedCallback() {
        this.handleDialogDisplay(false)
        this.removeSelectedEvent.emit();
    }

    handleDialogDisplay(show) {
        if (this.allowDelete) {
            this.showConfirmationDialog = show
        }
    }

    saveMenu() {
        this.saveMenuEvent.emit();
    }

    private sendItemToHandle(itemToHandle: menuItemClass, contentType: modalContentType): void {
        itemToHandle.action = contentType;
        this.displayService.setDisplay(itemToHandle);
    }


}