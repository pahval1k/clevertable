import {modalContentType} from '../enums/modal.conent.type'
import {modalContentClass} from '../classes/modalContentClass'

export class ModalContentFactory{

    modalContentType: modalContentType;

    constructor(modalContentType: modalContentType) {
        this.modalContentType = modalContentType;
    }

    public getModalContent = () => {
        let modalVisible: boolean = true;
        let title: string = "";
        let body: string = "";
        let confirmButtonText: string = "";
        let rejectButtonText: string = "";

        switch (this.modalContentType) {
            case 0:
                title = "Are you sure you want to delete item?";
                confirmButtonText = "Remove";
                rejectButtonText = "Cancel";
                break;
            case 1:
                title = "add item";
                confirmButtonText = "Add";
                rejectButtonText = "Cancel";
                break;
            case 2:
                title = "review item";
                rejectButtonText = "Ok";
                confirmButtonText = null;
                break;
            case 3:
                title = "edit item";
                confirmButtonText = "Save";
                rejectButtonText = "Cancel";
                break;

        }

        let modalContentInstance = new modalContentClass(modalVisible, title, body, confirmButtonText, rejectButtonText, this.modalContentType);
        return modalContentInstance;
    }

}