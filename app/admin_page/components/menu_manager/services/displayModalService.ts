import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { menuItemClass } from '../classes/menuItemClass';


@Injectable()
export class DisplayModalService {

    private subject: Subject<menuItemClass> = new Subject<menuItemClass>();
    private modalContent: menuItemClass;

    setDisplay(modalContent: menuItemClass): void {
        this.modalContent = modalContent;
        this.subject.next(modalContent);
    }

    getDisplay(): Observable<menuItemClass> {
        return this.subject.asObservable();
    }
}