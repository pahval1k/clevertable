import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { menuItemClass } from '../classes/menuItemClass';
import MenuTransform from '../transforms/menuTransform';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class MenuService {

    entireMenuData: Array<any>;
    entireMenuDataLine: Array<any>;
    lastId: number;
    menuTransform: MenuTransform;

    constructor(private http:Http) {
        this.lastId = 0;
        this.menuTransform = new MenuTransform()
        this.entireMenuData = [
            {
                "_id": 1,
                "title": "ASDF Signed in as Mark Otto1",
                "description": "Lorem ipsum dolor sit amet, consectetur",
                "imagePath": "somepath",
                "categoryId": 1,
                "inheritanceLevel": 1,
                "subItemOf": 0,
                "subItems": [
                    {
                        "_id": 2,
                        "title": "ASDFGG Signed in as Mark Otto",
                        "description": "Lorem ipsum dolor sit amet, consectetur",
                        "price": "13",
                        "imagePath": "somepath",
                        "categoryId": 0,
                        "inheritanceLevel": 2,
                        "subItemOf": 1
                    },
                    {
                        "_id": 3,
                        "title": "QWERT Signed in as Mark Otto",
                        "description": "Lorem ipsum dolor sit amet, consectetur",
                        "price": "14",
                        "imagePath": "somepath",
                        "categoryId": 0,
                        "inheritanceLevel": 2,
                        "subItemOf": 1
                    }
                ]
            },
            {
                "_id": 4,
                "title": "Signed in as Mark Otto",
                "description": "Lorem ipsum dolor sit amet, consectetur",
                "imagePath": "somepath",
                "categoryId": 1,
                "inheritanceLevel": 1,
                "subItemOf": 0,
                "subItems": [
                    {
                        "_id": 5,
                        "title": "Signed in as Mark Otto",
                        "description": "Lorem ipsum dolor sit amet, consectetur",
                        "imagePath": "somepath",
                        "categoryId": 2,
                        "inheritanceLevel": 2,
                        "subItemOf": 4,
                        "subItems": [
                            {
                                "_id": 6,
                                "title": "Signed in as Mark Otto",
                                "description": "Lorem ipsum dolor sit amet, consectetur",
                                "price": "13",
                                "imagePath": "somepath",
                                "categoryId": 0,
                                "inheritanceLevel": 3,
                                "subItemOf": 5
                            },
                            {
                                "_id": 7,
                                "title": "Signed in as Mark Otto",
                                "description": "Lorem ipsum dolor sit amet, consectetur",
                                "price": "13",
                                "imagePath": "somepath",
                                "categoryId": 0,
                                "inheritanceLevel": 3,
                                "subItemOf": 5
                            }
                        ]
                    },
                    {
                        "_id": 8,
                        "title": "Signed in as Mark Otto",
                        "description": "Lorem ipsum dolor sit amet, consectetur",
                        "price": "14",
                        "imagePath": "somepath",
                        "categoryId": 2,
                        "inheritanceLevel": 2,
                        "subItemOf": 4
                    }
                ]
            }
        ];

        this.entireMenuDataLine = [
            {
                "_id": 1,
                "title": "ASDF Signed in as Mark Otto1",
                "description": "Lorem ipsum dolor sit amet, consectetur",
                "imagePath": "somepath",
                "categoryId": 1,
                "inheritanceLevel": 1,
                "subItemOf": 0,
                "subItems": []
            },
            {
                "_id": 2,
                "title": "ASDFGG Signed in as Mark Otto",
                "description": "Lorem ipsum dolor sit amet, consectetur",
                "price": "13",
                "imagePath": "somepath",
                "categoryId": 0,
                "inheritanceLevel": 2,
                "subItemOf": 1
            },
            {
                "_id": 3,
                "title": "QWERT Signed in as Mark Otto",
                "description": "Lorem ipsum dolor sit amet, consectetur",
                "price": "14",
                "imagePath": "somepath",
                "categoryId": 0,
                "inheritanceLevel": 2,
                "subItemOf": 1
            },
            {
                "_id": 4,
                "title": "Signed in as Mark Otto",
                "description": "Lorem ipsum dolor sit amet, consectetur",
                "imagePath": "somepath",
                "categoryId": 1,
                "inheritanceLevel": 1,
                "subItemOf": 0,
                "subItems": []
            },
            {
                "_id": 5,
                "title": "Signed in as Mark Otto",
                "description": "Lorem ipsum dolor sit amet, consectetur",
                "imagePath": "somepath",
                "categoryId": 2,
                "inheritanceLevel": 2,
                "subItemOf": 4,
                "subItems": []
            },
            {
                "_id": 6,
                "title": "Signed in as Mark Otto",
                "description": "Lorem ipsum dolor sit amet, consectetur",
                "price": "13",
                "imagePath": "somepath",
                "categoryId": 0,
                "inheritanceLevel": 3,
                "subItemOf": 5
            },
            {
                "_id": 7,
                "title": "Signed in as Mark Otto",
                "description": "Lorem ipsum dolor sit amet, consectetur",
                "price": "13",
                "imagePath": "somepath",
                "categoryId": 0,
                "inheritanceLevel": 3,
                "subItemOf": 5
            },
            {
                "_id": 8,
                "title": "Signed in as Mark Otto",
                "description": "Lorem ipsum dolor sit amet, consectetur",
                "price": "14",
                "imagePath": "somepath",
                "categoryId": 2,
                "inheritanceLevel": 2,
                "subItemOf": 4
            }
        ];
    }

    getEntireMenuLine() {

        let menuItems: Array<menuItemClass>;

        menuItems = this.menuTransform.entireMenuTransformLine(this.entireMenuDataLine);

        return menuItems;
    }

    search() {
        return this.http
            .get(`/getEntireMenu`)
            .map((response) => {
                return this.menuTransform.entireMenuTransformLine(response.json())
            })
            .toPromise();
    }

    saveMenu(menuItems) {
        //console.log(menuItems)
        var json = JSON.stringify(menuItems);
        var params = 'menu_items=' + json;

        return this.http
            .post('/saveMenu', params )
            .map((response) => {
                return response.json()
            }).toPromise();
    }

    editMenuItem(menuItem) {
        var json = JSON.stringify(menuItem);
        var params = 'item_to_edit=' + json;

        return this.http
            .post('/updateItem', params )
            .map((response) => {
                return response.json()
            }).toPromise();
    }
    getLastId() {
        return this.lastId;
    }
}