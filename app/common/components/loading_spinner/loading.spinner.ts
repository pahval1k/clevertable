import { Component, Input } from '@angular/core';

@Component({
    selector: 'loading-spinner',
    templateUrl: 'app/common/components/loading_spinner/loading.spinner.html',
    styleUrls: ['app/common/components/loading_spinner/loading.spinner.css']
})

export class LoadingSpinner {
    @Input()
    isLoading: boolean;

    constructor() {
        this.isLoading = false
    }
}
