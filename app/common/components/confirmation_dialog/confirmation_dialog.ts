import { Component, Input } from '@angular/core';
import { SimpleChanges } from '@angular/core';

@Component({
    selector: 'confirmation-dialog',
    templateUrl: 'app/common/components/confirmation_dialog/confirmation_dialog.html',
    styleUrls: ['app/common/components/confirmation_dialog/confirmation_dialog.css']
})

export class ConfirmationDialog {

    @Input()
    okCallback: Function;
    @Input()
    rejectCallback: Function;
    @Input()
    showDialog: boolean;
    @Input()
    title: string;

    constructor() {
    }
}
