import { Injectable } from '@angular/core';
import { RequestOptions, RequestOptionsArgs } from '@angular/http';
import {Headers} from '@angular/http';

@Injectable()
export class CustomRequestOptions extends RequestOptions {
    merge(options?: RequestOptionsArgs): RequestOptions {
        let headers = new Headers();
        if (options !== null && options.url !== null) {
            options.url = 'http://localhost:8000' + options.url;
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            options.headers = headers;
        }
        let requestOptions = super.merge(options)
        return new CustomRequestOptions({
            method: requestOptions.method,
            url: requestOptions.url,
            search: requestOptions.search,
            headers: requestOptions.headers,
            body: requestOptions.body,
            withCredentials: requestOptions.withCredentials,
            responseType: requestOptions.responseType
        });
    }
}